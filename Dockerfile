FROM node:8.9.4
LABEL maintainer="KatIao <kat.iao@ctigroup.hk>"

# Build time variable
RUN mkdir /workspace/ -p
COPY . /workspace/
WORKDIR /workspace/


# Install app dependencies
RUN npm install

CMD [ "npm", "start" ]
