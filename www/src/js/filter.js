(function ($) {
    'use strict';
    $('.constraint').hide();
    $('.daterangepicker').hide();
    $('#idplatform').on('change', function(){
      if (this.value == 'clientid'){
        $('#idnum').attr('placeholder', 'Start with Client_...');
      }else{
        $('#idnum').attr('placeholder', 'Start with Wechat_...');
      }
    })
    /*==================================================================
        [ Daterangepicker ]*/


    try {
        $('#input-start').daterangepicker({
            ranges: true,
            autoApply: true,
            applyButtonClasses: false,
            autoUpdateInput: false
        },function (start, end) {
            $('#input-start').val(start.format('YYYY-MM-DD'));
            $('#input-end').val(end.format('YYYY-MM-DD'));
        });

        $('#input-start-2').daterangepicker({
            ranges: true,
            autoApply: true,
            applyButtonClasses: false,
            autoUpdateInput: false
        },function (start, end) {
            $('#input-start-2').val(start.format('YYYY-MM-DD'));
            $('#input-end-2').val(end.format('YYYY-MM-DD'));
        });

    } catch(er) {console.log(er);}
    /*==================================================================
        [ Single Datepicker ]*/


    try {
        var singleDate = $('.js-single-datepicker');

        singleDate.each(function () {
            var that = $(this);
            var dropdownParent = '#dropdown-datepicker' + that.data('drop');

            that.daterangepicker({
                "singleDatePicker": true,
                "showDropdowns": true,
                "autoUpdateInput": true,
                "parentEl": dropdownParent,
                "opens": 'left',
                "locale": {
                    "format": 'YYYY-MM-DD'
                }
            });
        });

    } catch(er) {console.log(er);}
    /*==================================================================
        [ Special Select ]*/

    try {
        var body = $('body,html');

        var selectSpecial = $('#js-select-special');
        var info = selectSpecial.find('#info');
        var dropdownSelect = selectSpecial.parent().find('.dropdown-select');
        var listRoom = dropdownSelect.find('.list-room');
        var btnAddRoom = $('#btn-add-room');
        var totalRoom = 1;

        selectSpecial.on('click', function (e) {
            e.stopPropagation();
            $(this).toggleClass("open");
            dropdownSelect.toggleClass("show");
        });

        dropdownSelect.on('click', function (e) {
            e.stopPropagation();
        });

        body.on('click', function () {
            selectSpecial.removeClass("open");
            dropdownSelect.removeClass("show");
        });

        function isNumber(n){
            return typeof(n) != "boolean" && !isNaN(n);
        }


    } catch (e) {
        console.log(e);
    }
    /*[ Select 2 Config ]
        ===========================================================*/

    try {
        var selectSimple = $('.js-select-simple');

        selectSimple.each(function () {
            var that = $(this);
            var selectBox = that.find('select');
            var selectDropdown = that.find('.select-dropdown');
            selectBox.select2({
                dropdownParent: selectDropdown
            });
        });

    } catch (err) {
        console.log(err);
    }


})(jQuery);
