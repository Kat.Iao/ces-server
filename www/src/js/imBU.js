var name = 'agent';
var socket;
var name;
var sendingFile = false;
// const callButton = document.getElementById('callButton');
// const hangupButton = document.getElementById('hangupButton');
const callbtn = document.getElementById('vcallStatus');
var SocketData = (function (){
	return {
		getAgentName: function(){
			return name;
		},
		getClientID: function(){
			return $('.chat-active').attr('data-n');
		},
		callAction: function(){
				// $('#vcallStatus').attr('src', 'src/img/hangup.png');
				// $('#vcallStatus').attr('class', 'hangup');
				// callbtn.className = 'hangup';
				// callbtn.src = 'src/img/hangup.png';
				$('#vcallStatus').fadeOut();
				$('.cancelcall').fadeOut();
				$('.hangup').fadeIn();
				console.log('Starting call.');
				startTime = window.performance.now();
				var data={
					recipient: $('.chat-active').attr('data-n'),
					addresser: name
				}

				socket.emit('video calling',data);
		}
	}
}());

console.log('123')
$.get('/cookies', (data)=>{
	data = JSON.parse(data)
	name = data.username

		// socket = io.connect('http://ces-server.ap-southeast-2.elasticbeanstalk.com:3001');
		socket = io.connect('http://localhost:3001');
		console.log(name)
		$('h1').text(name);
		var USE_AUDIO = true;
    var USE_VIDEO = true;
    var DEFAULT_CHANNEL = 'channel-'+name;
    var MUTE_AUDIO_BY_DEFAULT = false;
    var local_media_stream = null; /* our own microphone / webcam */
    var peers = {};                /* keep track of our peer connections, indexed by peer_id (aka socket.io id) */
    var peer_media_elements = {};  /* keep track of our <video>/<audio> tags, indexed by peer_id */
		var track;

    /** You should probably use a different stun server doing commercial stuff **/
    /** Also see: https://gist.github.com/zziuni/3741933 **/
    var ICE_SERVERS = [
        {url:"stun:stun.l.google.com:19302"}
    ];

		socket.on('connecting', function () {
			console.log('connecting');
		});


		socket.on('connect', function () {
			console.log('connect');

			if(name){
				console.log("emit:: ", name)
				socket.emit('new agent', name);
			}
			$('.outline').css('display','block');
		});

		socket.on('login', function (agent, user, queuing, token, tokenAgent, agents) {
			//console.log(history);
			console.log("access login socket function");
			$('.outline').css('display','none');
			incomeHtml('session',agent, 'src/img/head.jpg');
			$('#user-amount').text(user.length);
			$('#request-amount').text(queuing.length);
			$('#token-amount').text(token.length);
			// $('#dashboard').contents().find('#inservice').text(queuing.length+token.length+user.length);
			// $('#dashboard').contents().find('#usageamount').text(historyid.length);
			// $('#dashboard').contents().find('#liveamount').text(requested);
			if(user.length>=1){
				for(var i =0;i<user.length;i++){
					incomeHtml('chatbotuser', user[i],'src/img/client.jpg', null);
				}
			}
			if(queuing.length>=1){
				for(var i =0;i<queuing.length;i++){
					incomeHtml('queuinguser', queuing[i],'src/img/client.jpg', null);
				}
			}
			if(token.length>=1){
				for(var i =0;i<token.length;i++){
					console.log(tokenAgent[token[i]]);
					incomeHtml('tokenuser', token[i], 'src/img/client.jpg', null,tokenAgent[token[i]]);
				}
			}
			updateAgentList(agents);
			// if(historyid.length>=1){
			// 	for(var i=0; i<historyid.length; i++){
			// 		console.log(history[historyid[i]]);
			// 		incomeHtml('historyuser', historyid[i], 'src/img/client.jpg', history);
			// 	}
			// }
		});
		// socket.on('agent joined', function (tname, index) {
		// 	incomeHtml(tname,'src/img/head.jpg');
		// //	console.log(tname+' joined');
		// //	showNotice('src/img/client.jpg',tname," joined");
		// });
		socket.on('update agent list', function(agents){
			updateAgentList(agents);
		})

		socket.on('client joined', function (tname, index, users, currentAmount) {
			incomeHtml('chatbotuser',tname,'src/img/client.jpg');
			console.log(tname+' joined');
			showNotice('src/img/client.jpg',tname," joined");
			$('#user-amount').text(users.length);
			$('#dashboard').contents().find('#inservice').text(currentAmount);
		});

		socket.on('load request', function (clientname, users, queue) {
		//	$('#'+hex_md5(clientname)).remove();
			$('#li'+hex_md5(clientname)).appendTo('#queuinguser');
			$('#dashboard').contents().find('#liveamount').text(parseInt($('#dashboard').contents().find('#liveamount').text())+1);
		//	var html = '<span id="request" style="float:right; font-size:x-large;">&#10071;</span>';
		//	$('.notifications .counter').addClass('bgc-red');
			notifications(queue.length);
			$('#request-amount').text(queue.length);
			$('#user-amount').text(users.length);
		//	incomeHtml('queuinguser',clientname, 'src/img/client.jpg');

		})

		socket.on('receive private message', function (data) {
	//		$('#ding')[0].play();
			var ts = new Date;
			if(data.addresser == data.recipient) return;
			var head = 'src/img/client.jpg';
			$('#'+hex_md5(data.addresser)+' .chat-msg').append('<li><img src="'+head+'"><span class="speak">'+data.body+'</span><p class="help-block">'+ts.toLocaleTimeString()+'</p></li>');
			if(document.hidden){
				showNotice(head,data.addresser,data.body);
			}
			scrollToBottom(hex_md5(data.addresser));
		});

		socket.on('refresh amount', function (users, queue, token){
			notifications(queue.length);
			$('#request-amount').text(queue.length);
			$('#token-amount').text(token.length);
			$('#user-amount').text(users.length);
		})

		socket.on('load chatbot message', function (data){
			console.log("access in im.js in server");
			loadMessage('src/img/head.jpg',data);
			scrollToBottom(hex_md5(data.recipient));
		});

		socket.on('prevent duplicate take', function (data, users, queue, token){

			notifications(queue.length);
			$('#request-amount').text(queue.length);
			$('#token-amount').text(token.length);
			$('#user-amount').text(users.length);
			if (data.status == 'takeover'){
			//	$('#'+hex_md5(data.recipient)).remove();
				$('#li'+hex_md5(data.recipient)).appendTo('#tokenuser');
			//	incomeHtml('tokenuser', data.recipient, 'src/img/client.jpg');
				$('#'+hex_md5(data.recipient)+' #takeoverBtn').addClass("isDisabled").removeClass("takeover").text("Token already");
				$('#request').remove();
				var load ={
					'recipient': data.recipient,
					'addresser': data.addresser,
					'body': '客服人員'+data.addresser+'已接線'
				}
				loadMessage('src/img/head.jpg', load);
			}else if (data.status == 'hangup'){
				// $('#'+hex_md5(data.recipient)+' #takeoverBtn').removeClass("isDisabled").addClass("takeover").text("TakeOver");
		//		$('#'+hex_md5(data.recipient)).remove();
				$('#li'+hex_md5(data.recipient)).appendTo('#chatbotuser');
			//	incomeHtml('chatbotuser', data.recipient, 'src/img/client.jpg');
				var load ={
					'recipient': data.recipient,
					'addresser': data.addresser,
					'body': '客服人員'+data.addresser+'已掛線'
				}
				loadMessage('src/img/head.jpg', load);
			}

		});

		socket.on('addPeer', function(config) {
	      console.log('Signaling server said to add peer:', config);
	      var peer_id = config.peer_id;
	      if (peer_id in peers) {
	          /* This could happen if the user joins multiple channels where the other peer is also in. */
	          console.log("Already connected to peer ", peer_id);
	          return;
	      }
	      var peer_connection = new RTCPeerConnection(
	          {"iceServers": ICE_SERVERS},
	          {"optional": [{"DtlsSrtpKeyAgreement": true}]} /* this will no longer be needed by chrome
	                                                          * eventually (supposedly), but is necessary
	                                                          * for now to get firefox to talk to chrome */
	      );
	      peers[peer_id] = peer_connection;

	      peer_connection.onicecandidate = function(event) {
	          if (event.candidate) {
	              socket.emit('relayICECandidate', {
	                  'peer_id': peer_id,
	                  'ice_candidate': {
	                      'sdpMLineIndex': event.candidate.sdpMLineIndex,
	                      'candidate': event.candidate.candidate
	                  }
	              });
	          }
	      }
	      peer_connection.onaddstream = function(event) {
	          console.log("onAddStream", event);
	          var remote_media = USE_VIDEO ? $("<video>") : $("<audio>");
	          remote_media.attr("autoplay", "autoplay");
	          if (MUTE_AUDIO_BY_DEFAULT) {
	              remote_media.attr("muted", "true");
	          }
	          remote_media.attr("controls", "");
						remote_media.attr("id", "remoteVideo");
	          peer_media_elements[peer_id] = remote_media;
	          $('.videochat').append(remote_media);
	          attachMediaStream(remote_media[0], event.stream);
	      }

	      /* Add our local stream */
	      peer_connection.addStream(local_media_stream);

	      /* Only one side of the peer connection should create the
	       * offer, the signaling server picks one to be the offerer.
	       * The other user will get a 'sessionDescription' event and will
	       * create an offer, then send back an answer 'sessionDescription' to us
	       */
	      if (config.should_create_offer) {
	          console.log("Creating RTC offer to ", peer_id);
	          peer_connection.createOffer(
	              function (local_description) {
	                  console.log("Local offer description is: ", local_description);
	                  peer_connection.setLocalDescription(local_description,
	                      function() {
	                          socket.emit('relaySessionDescription',
	                              {'peer_id': peer_id, 'session_description': local_description});
	                          console.log("Offer setLocalDescription succeeded");
	                      },
	                      function() { Alert("Offer setLocalDescription failed!"); }
	                  );
	              },
	              function (error) {
	                  console.log("Error sending offer: ", error);
	              });
	      }
	  });

		socket.on('iceCandidate', function(config) {
        var peer = peers[config.peer_id];
        var ice_candidate = config.ice_candidate;
        peer.addIceCandidate(new RTCIceCandidate(ice_candidate));
    });

		socket.on('relayICECandidate', function(config) {
        var peer_id = config.peer_id;
        var ice_candidate = config.ice_candidate;
        console.log("["+ socket.id + "] relaying ICE candidate to [" + peer_id + "] ", ice_candidate);

        if (peer_id in sockets) {
          sockets[peer_id].emit('iceCandidate', {'peer_id': socket.id, 'ice_candidate': ice_candidate});
        }
    });

		socket.on('sessionDescription', function(config) {
        console.log('Remote description received: ', config);
        var peer_id = config.peer_id;
        var peer = peers[peer_id];
        var remote_description = config.session_description;
        console.log(config.session_description);

        var desc = new RTCSessionDescription(remote_description);
        var stuff = peer.setRemoteDescription(desc,
            function() {
                console.log("setRemoteDescription succeeded");
                if (remote_description.type == "offer") {
                    console.log("Creating answer");
                    peer.createAnswer(
                        function(local_description) {
                            console.log("Answer description is: ", local_description);
                            peer.setLocalDescription(local_description,
                                function() {
                                    socket.emit('relaySessionDescription',
                                        {'peer_id': peer_id, 'session_description': local_description});
                                    console.log("Answer setLocalDescription succeeded");
                                },
                                function() { Alert("Answer setLocalDescription failed!"); }
                            );
                        },
                        function(error) {
                            console.log("Error creating answer: ", error);
                            console.log(peer);
                        });
                }
            },
            function(error) {
                console.log("setRemoteDescription error: ", error);
            }
        );
        console.log("Description Object: ", desc);

    });

		socket.on('iceCandidate', function(config) {
        var peer = peers[config.peer_id];
        var ice_candidate = config.ice_candidate;
        peer.addIceCandidate(new RTCIceCandidate(ice_candidate));
    });

		socket.on('vcallhangup', function(data){
			socket.emit('p90art', data);
			initLocalMedia();
			$('.hangup').fadeOut();
			$('#vcallStatus').fadeIn();
		});

		socket.on('removePeer', function(config) {
        console.log('Signaling server said to remove peer:', config);
        // var peer_id = config.peer_id;
        // if (peer_id in peer_media_elements) {
        //     peer_media_elements[peer_id].remove();
        // }
        // if (peer_id in peers) {
        //     peers[peer_id].close();
        // }
        // delete peers[peer_id];
        // delete peer_media_elements[config.peer_id];
				for (peer_id in peer_media_elements) {
              peer_media_elements[peer_id].remove();
              delete peer_media_elements[peer_id];
          }
          for (peer_id in peers) {
              peers[peer_id].close();
              delete peers[peer_id];
          }
					$('.videochat').fadeOut();
    });

		socket.on('client left', function (data,users, queuing, token, historyid, singlehistory) {
			console.log(data+' left');
			incomeHtml('historyuser', historyid, 'src/img/client.jpg', singlehistory);
			$('#user-amount').text(users.length);
			$('#request-amount').text(queuing.length);
			$('#token-amount').text(token.length);
			$('#dashboard').contents().find('#inservice').text(queuing.length+token.length+users.length);
			$('#'+hex_md5(data)).remove();
			$('#li'+hex_md5(data)).remove();
		});

		socket.on('disconnect', function () {
			$('.outline').css('display','block');
			$('.customer').children().remove();
			$('#chat').children().remove();
			for (peer_id in peer_media_elements) {
					peer_media_elements[peer_id].remove();
			}
			for (peer_id in peers) {
					peers[peer_id].close();
			}
			peers = {};
			peer_media_elements = {};
			console.log('you have been disconnected');
		});

		socket.on('reconnect', function () {
			console.log('you have been reconnected');
			$('.outline').css('display','none');
		});


		socket.on('reconnect_error', function () {
			console.log('attempt to reconnect has failed');
		});

		socket.on('file sent', function(){
			sendingFile = false;
      $('.input.active').val('');
			$('.input.active').removeAttr("disabled");
			$('.send.active').removeClass("isDisabled").attr("href", "javascript:void(0)");
		})

		$(document).ready(function () {
		$(".popup").hide();
		$(".openpop").click(function (e) {
			e.preventDefault();
			$(".popup").fadeIn('slow');
		});
			$(".openchat").click(function (e) {
			e.preventDefault();
			$(".popup").fadeOut('slow');
		});
	});
	//
	// socket.on('wait upload finish', function(client){
	// 	loadFileMsg("src/img/client.jpg", )
	// 	$('#'+hex_md5(client)+' .chat-msg').append('<li><img src="src/img/client.jpg"><span class="speak">'+'<img class="imageMsg" src="public/upload/'+val.name+'" onclick="window.open($(this)[0].src, \'_blank\')">'+'</span><p class="help-block">'+ (val.date ? ts : ts.toLocaleTimeString())+'</p></li>');
	// 	scrollToBottom(hex_md5(val.addresser));
	// })

	socket.on('end upload', function(file){
		loadFileMsg("src/img/client.jpg", file);
	})


		//sendMessage
		$(document).on('click','.chat-active .send',function(){
			var recipient = $('.chat-active').attr('data-n');
			var val = $('.chat-active input').val();
			if(val == '') return;
			sendMessage('src/img/head.jpg',val,name);
			//callhangup
			var req = {
				'addresser':name,
				'recipient':recipient,
				'type':'plain',
				'body':val
			}
			socket.emit('send private message', req);
			$('.chat-active input').val('');
			scrollToBottom(hex_md5(recipient));
		});


		// $(document).on('click', '.chat-active .takeover', function(){
		// 	$('.chat-active > .box :input').removeAttr("disabled");
		// 	$('.chat-active >.send').removeClass("isDisabled").attr("href", "javascript:void(0)");
		// 	$('.chat-active > #takeoverBtn').removeClass("takeover").addClass("close");
		// });
		$(document).on('click', '.chat-active .takeover', function(){
			var recipient = $('.chat-active').attr('data-n');
			// $('#token-amount').text(parseInt($('#token-amount').text())+1);
			// $('#request-amount').text(parseInt($('#request-amount').text())-1);
		//	var html = '<a class="transfer" id="transferBtn" href="javascript:void(0)">Transfer to</a>';
			$('#li'+hex_md5(recipient)).appendTo('#tokenuser');
		//	$('#'+hex_md5(recipient)+' .head').append(html);
			$('#request').remove();
			$('.input.active').removeAttr("disabled");
			$('.send.active').removeClass("isDisabled").attr("href", "javascript:void(0)");
			$('.transfer').fadeIn();
			$(this).removeClass("takeover").addClass("livehangup");
			$(this).text("HangUp");
			var req = {
				'addresser':name,
				'recipient':recipient,
				'status':'takeover'
			}
			sendMessage('src/img/head.jpg','客服人員'+name+'已接線',name);
			scrollToBottom(hex_md5(recipient));
			socket.emit('takeover', req);
		});

		$(document).on('click', '.chat-active .livehangup', function(){
			// $('#token-amount').text(parseInt($('#token-amount').text())-1);
			// $('#user-amount').text(parseInt($('#user-amount').text())+1);
			var recipient = $('.chat-active').attr('data-n');
			$('#li'+hex_md5(recipient)).appendTo('#chatbotuser');
			$('.input.active').attr("disabled", true);
			$('.send.active').addClass("isDisabled").attr("href", "");
			$('.transfer').fadeOut();
			$(this).addClass("takeover").removeClass("livehangup");
			$(this).text("TakeOver");
			var req = {
				'addresser':name,
				'recipient':recipient,
				'status':'hangup'
			}
			sendMessage('src/img/head.jpg','客服人員'+name+'已離線',name);
			scrollToBottom(hex_md5(recipient));
			socket.emit('takeover', req);
		});

		$(document).on('click', '.chat-active .transfer', function(){
			$('#transferAgent').fadeIn();
		})

		$(document).on('click', '.collapsible-menu' ,function(){
			$('.menuactive').removeClass('menuactive');
			$(this).addClass('menuactive');
		})

		document.onkeydown = function(e){
			if(e && e.keyCode == 13) {
				var recipient = $('.chat-active').attr('data-n');
				var val = $('.chat-active input').val();
				if(val == '') return;
				sendMessage('src/img/head.jpg',val,name);
				//call
				var req = {
					'addresser':name,
					'recipient':recipient,
					'type':'plain',
					'body':val
				}
				socket.emit('send private message', req);
				$('.chat-active input').val('');
				scrollToBottom(hex_md5(recipient));
			}
		}

		//active li
		$(document).on('click','.customer li',function(){
			$('.active').removeClass('active');
			$(this).addClass('active');
			var id = $(this).attr('id').substring(2);
	//		console.log(id);
		//	id=id.substring(2);
			$('.send').removeClass('active');
			$('.input').removeClass('active');
			$('.chat-active').removeClass('chat-active');
			$('#'+id).addClass('chat-active');
			$('#'+id+' .input').addClass('active');
			$('#'+id+' .send').addClass('active');
		});

		$(document).on('click','.chat-active .emoji',function(){
			if ( $('.input.active').attr('disabled') == true || $('.input.active').attr('disabled') == "disabled"){
				console.log("disable");
			}else{
				if ($('#emoji').css('display') == 'block'){
					// $('#emoji').css('display','none');
					$('#emoji').fadeOut();
				}else{
					// $('#emoji').css('display','block');
					$('#emoji').fadeIn();
				}
				if ($('#demands').css('display') == 'block')	$('#demands').fadeOut();
			}
			// if ( $('#replyfield').attr('disabled') != true || $('#replyfield').attr('disabled') != "disabled"){
			// 	console.log("disable!");
			// 	if ($('#emoji').css('display') == 'block'){
			// 		console.log("emoji is block before")
			// 		$('#emoji').css('display') == 'none'
			// 	}else{
			// 		console.log("emoji is not show before")
			// 		$('#emoji').css('display','block');
			// 	}
			// }else{
			// 	console.log("enable");

		});

		$(document).on('click', '.demand', function(){
			if ( $('.input.active').attr('disabled') == true || $('.input.active').attr('disabled') == "disabled"){
				console.log("disable");
			}else{
				if ($('#demands').css('display') == 'block'){
					// $('#emoji').css('display','none');
					$('#demands').fadeOut();
				}else{
					// $('#emoji').css('display','block');
					$('#demands').fadeIn();
				}
				if ($('#emoji').css('display') == 'block')	$('#emoji').fadeOut();
			}
		})

		$(document).on('click', '.startButton', function(){
		//	console.log("access click jquery");
			if ( $('.input.active').attr('disabled') == true || $('.input.active').attr('disabled') == "disabled"){
				console.log("disable");
			}else{
					$('.videochat').fadeIn();
					console.log("Connected to signaling server");
	        setup_local_media(function() {
	            /* once the user has given us access to their
	             * microphone/camcorder, join the channel and start peering up */
	            join_chat_channel(DEFAULT_CHANNEL, {'whatever-you-want-here': 'stuff'});
	        });
			}
		});

		$(document).on('click', '.livehangup', function(){
			var data = {
				recipient: $('.chat-active').attr('data-n'),
				channel: 'channel-'+ name
			};
			socket.emit('part', data);
			$('.hangup').fadeOut();
			$('#vcallStatus').fadeIn();

			$('.cancelcall').show();
			initLocalMedia();
		});

		$(document).on('click', '.cancelcall', function(){
			$('.videochat').fadeOut();
			initLocalMedia();
		})

		$(document).on('click', '.selectfile', function(){
			if ( $('.input.active').attr('disabled') == true || $('.input.active').attr('disabled') == "disabled"){
				console.log("disable");
			}else{
				$('#uploadfile').click();
			}
		});

		$(document).on('change', '#uploadfile', function(e){
			console.log("file sending");
			var data = e.originalEvent.target.files[0];
				readThenSendFile(data);
		})
		//
		// $('#uploadfile').change(function(e){
		// 	console.log("file sending");
	  //   var data = e.originalEvent.target.files[0];
		//     readThenSendFile(data);
		// });

		// function readThenSendFile(data){
		//
		//     var reader = new FileReader();
		//     reader.onload = function(evt){
		//         var msg ={};
		// 				msg.addresser = name;
		//         msg.recipient = $('.chat-active').attr('data-n');
		// 				msg.type = 'file';
		//         msg.file = evt.target.result;
		//         msg.fileName = data.name;
		//         socket.emit('base64 file', msg);
		// 				uploadFile(evt.target.result);
		// 				loadFileMsg('src/img/head.jpg', msg);
		//     };
		//     reader.readAsDataURL(data);
		// }

		function setup_local_media(callback, errorback) {
      if (local_media_stream != null) {  /* ie, if we've already been initialized */
          if (callback) callback();
          return;
      }
      /* Ask user for permission to use the computers microphone and/or camera,
       * attach it to an <audio> or <video> tag if they give us access. */
      console.log("Requesting access to local audio / video inputs");

      navigator.getUserMedia = ( navigator.getUserMedia ||
             navigator.webkitGetUserMedia ||
             navigator.mozGetUserMedia ||
             navigator.msGetUserMedia);

      attachMediaStream = function(element, stream) {
          console.log('DEPRECATED, attachMediaStream will soon be removed.');
          element.srcObject = stream;
       };

      navigator.getUserMedia({"audio":USE_AUDIO, "video":USE_VIDEO},
          function(stream) { /* user accepted access to a/v */
              console.log("Access granted to audio/video");
              local_media_stream = stream;
              var local_media = USE_VIDEO ? $("<video>") : $("<audio>");
              local_media.attr("autoplay", "autoplay");
							local_media.attr("id", "localVideo");
              local_media.attr("muted", "true"); /* always mute ourselves by default */
              local_media.attr("controls", "");
              $('.vselflayer').append(local_media);
							console.log("appending video.");
              attachMediaStream(local_media[0], stream);

              if (callback) callback();
          },
          function() { /* user denied access to a/v */
              console.log("Access denied for audio/video");
              alert("You chose not to provide access to the camera/microphone, demo will not work.");
              if (errorback) errorback();
          });
  }

	function initLocalMedia(){
		if (local_media_stream !== null){
			local_media_stream.getTracks().forEach(track => track.stop());
			local_media_stream = null;
		}
	}


		function readThenSendFile(data){

				if(sendingFile){
						alert('Still sending last file!');
						return;
				}
				if(fileTooBig(data))
						return;
				var reader = new FileReader();
				reader.onload = function(evt){
						var msg ={};
						msg.addresser = name;
		        msg.recipient = $('.chat-active').attr('data-n');
						msg.type = 'file';
		        msg.file = evt.target.result;
		        msg.fileName = data.name;
						socket.emit('base64 file', msg);
						$('.input.active').val('Sending file...');
						sendingFile = true;
						$('.input.active').attr("disabled", true);
						$('.send.active').addClass("isDisabled").attr("href", "");
						loadFileMsg('src/img/head.jpg', msg);
				};
				reader.readAsDataURL(data);
		}


		function uploadFile(files){
			for (var i = 0; i < files.length; i++) {
          var file = files[i];
          var dateString = formatAMPM(new Date());
          var fd = new FormData();
				fd.append('file', file);
		  		fd.append('isPDFFile', true);
				fd.append('istype', "PDF");
				fd.append('dwimgsrc', "app/images/doc_icon.png");
				fd.append('msgTime', dateString);
				fd.append('filename', file.name);
				$.ajax({
					url: "/v1/uploadDoc",
					type: 'POST',
					data: fd,
					processData: false,
					enctype: 'multipart/form-data',
					contentType: false,
					dataType: 'json',
					success: function(data){
						console.log(data);
					}
				})
			  // $.post("/v1/uploadDoc", fd, {
	      //     headers: { 'Content-Type': undefined }
	      // }).then(function (response) {
	      //     //console.log(response);
	      // });
      }
		}
		function loadFileMsg(head, val){
			var ts = val.date || new Date;
			// console.log(hex_md5(val.recipient));
			// "<a target='_blank' download='" + val.name +"' href='/public/upload/"+val.name+"'>"+val.name+"</a>"
			if (val.recipient == name){
				$('#'+hex_md5(val.addresser)+' .chat-msg').append('<li><img src="'+head+'"><span class="speak">'+'<img class="imageMsg" src="public/upload/'+val.name+'" onclick="window.open($(this)[0].src, \'_blank\')">'+'</span><p class="help-block">'+ (val.date ? ts : ts.toLocaleTimeString())+'</p></li>');
				scrollToBottom(hex_md5(val.addresser));
			}else{
			$('#'+hex_md5(val.recipient)+' .chat-msg').append('<li><div class="icon"><img class="mehead" src="'+head+'"><span class="help-block">'+(val.addresser ? val.addresser : 'chatbot')+'</span></div><span class="mespeak">'+"<a target='_blank' download='" + val.fileName +"' href='"+val.file+"'>"+val.fileName+"</a>"+'</span><p class="help-block" style="float:right;">'+ (val.date ? ts : ts.toLocaleTimeString())+'</p></li>');
			}
		}
		function formatAMPM(date) {
			var hours = date.getHours();
			var minutes = date.getMinutes();
			var ampm = hours >= 12 ? 'pm' : 'am';
			hours = hours % 12;
			hours = hours ? hours : 12; // the hour '0' should be '12'
			minutes = minutes < 10 ? '0'+minutes : minutes;
			var strTime = hours + ':' + minutes + ' ' + ampm;
			return strTime;
		}

		function incomeHtml(usertype, tname, head, history, tokenAgent){
			$('#'+usertype).append('<li id="li'+hex_md5(tname)+'"><img src="'+head+'"><span class="nick-name">'+tname+'</span></li>');
			var html = '';

			html+='<div id="'+hex_md5(tname)+'" data-n="'+tname+'" class="chat"><div class="main">';

			if (usertype == 'tokenuser'){
				if (tokenAgent == name){
					html+='<div class="message"><div class="head"><p>'+tname+'</p><a class="transfer" id="transferBtn" href="javascript:void(0)">TransferTo</a><a class="livehangup" id="takeoverBtn" href="javascript:void(0)">HangUp</a></div>';
				}else{
					html+='<div class="message"><div class="head"><p>'+tname+'</p><a class="isDisabled" id="takeoverBtn" href="javascript:void(0)">Token already</a></div>';
				}
			}else{
				html+='<div class="message"><div class="head"><p>'+tname+'</p><a class="takeover" id="takeoverBtn" href="javascript:void(0)">TakeOver</a></div>';
			}

			html+='<div class="body"><ul class="chat-msg"></ul></div></div>';

			if (usertype=='historyuser'){
				$('#chat').append(html);
		//		console.log(history[tname].length);
				for (var i=0; i<history[tname].length; i++){
			//		console.log(i);
					if (history[tname][i].recipient == tname){
		//				console.log("to client");
						loadMessage('src/img/head.jpg', history[tname][i]);
					}else{
			//			console.log("from client");
						loadMessagefromClient('src/img/client.jpg', history[tname][i]);
					}
				}
			}else{
				html+='<div class="footer"><div class="box"><div class="head">';
				// html+='<a class="takeover" href="javascript:void(0)">TakeOver</a>';
				html+='<svg class="icon emoji" style="" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="4692" xmlns:xlink="http://www.w3.org/1999/xlink"><defs></defs><path d="M520.76544 767.05792c-99.14368 0-180.30592-73.65632-193.78176-169.09312l-49.22368 0c13.78304 122.624 116.61312 218.29632 242.91328 218.29632S749.81376 720.5888 763.5968 597.9648l-49.0496 0C701.0816 693.4016 619.90912 767.05792 520.76544 767.05792zM512 0C229.23264 0 0 229.2224 0 512c0 282.75712 229.23264 512 512 512 282.76736 0 512-229.24288 512-512C1024 229.2224 794.76736 0 512 0zM511.95904 972.78976C257.46432 972.78976 51.1488 766.48448 51.1488 512c0-254.49472 206.30528-460.81024 460.81024-460.81024 254.48448 0 460.8 206.30528 460.8 460.81024C972.75904 766.48448 766.44352 972.78976 511.95904 972.78976zM655.57504 456.92928c31.06816 0 56.24832-25.1904 56.24832-56.24832 0-31.06816-25.18016-56.24832-56.24832-56.24832-31.06816 0-56.25856 25.18016-56.25856 56.24832C599.31648 431.73888 624.49664 456.92928 655.57504 456.92928zM362.73152 456.92928c31.06816 0 56.24832-25.1904 56.24832-56.24832 0-31.06816-25.1904-56.24832-56.24832-56.24832-31.0784 0-56.25856 25.18016-56.25856 56.24832C306.47296 431.73888 331.65312 456.92928 362.73152 456.92928z" p-id="4693"></path></svg>';
				html+='<svg class="icon selectfile" version="1.1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 129 129" xmlns:xlink="http://www.w3.org/1999/xlink" enable-background="new 0 0 129 129"><g><path d="m60.4,58.2c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8,0l-21.7,21.7c-3.7,3.7-5.7,8.5-5.7,13.7 0,5.2 2,10 5.7,13.7 7.6,7.6 19.9,7.6 27.4,0l49-49c5.1-5.1 7.9-11.9 7.9-19.1s-2.8-14-7.9-19.1c-10.6-10.6-27.7-10.6-38.3,0l-48.9,48.9c-6.6,6.6-10.2,15.3-10.2,24.6 0,9.3 3.6,18.1 10.2,24.6 6.6,6.6 15.3,10.2 24.6,10.2 9.3,0 18.1-3.6 24.6-10.2l21.7-21.7c1.6-1.6 1.6-4.2 0-5.8s-4.2-1.6-5.8,0l-21.7,21.7c-5,5-11.7,7.8-18.8,7.8s-13.8-2.8-18.8-7.8-7.8-11.7-7.8-18.8 2.8-13.8 7.8-18.8l48.9-48.9c3.6-3.6 8.3-5.5 13.3-5.5 5,0 9.8,2 13.3,5.5 3.6,3.6 5.5,8.3 5.5,13.3s-2,9.8-5.5,13.3l-49,49c-4.3,4.3-11.4,4.3-15.8,0-2.1-2.1-3.3-4.9-3.3-7.9 0-3 1.2-5.8 3.3-7.9l21.8-21.7z"/></g></svg>';
				html+='<svg version="1.1" class="icon demand" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 462 462" style="enable-background:new 0 0 462 462;" xml:space="preserve"><g><path d="M342,251c19.299,0,35-15.701,35-35s-15.701-35-35-35s-35,15.701-35,35S322.701,251,342,251z M342,201c8.271,0,15,6.729,15,15s-6.729,15-15,15s-15-6.729-15-15S333.729,201,342,201z"/><path d="M342,261c-33.084,0-60,26.916-60,60c0,5.523,4.478,10,10,10s10-4.477,10-10c0-22.056,17.944-40,40-40s40,17.944,40,40c0,5.523,4.478,10,10,10s10-4.477,10-10C402,287.916,375.084,261,342,261z"/><path d="M30,324.189h-7.938c-0.852-1.564-2.062-5.02-2.062-10V97.811c0-5.514,4.486-10,10-10h352c4.98,0,8.436,1.21,10,2.062v7.938c0,5.523,4.478,10,10,10s10-4.477,10-10v-10c0-11.589-12.617-20-30-20H30c-16.542,0-30,13.458-30,30v216.379c0,17.383,8.411,30,20,30h10c5.522,0,10-4.477,10-10S35.522,324.189,30,324.189z"/><path d="M432,117.811H80c-16.542,0-30,13.458-30,30v216.379c0,16.542,13.458,30,30,30h352c16.542,0,30-13.458,30-30V147.811C462,131.269,448.542,117.811,432,117.811z M442,364.189c0,5.514-4.486,10-10,10H80c-5.514,0-10-4.486-10-10V147.811c0-5.514,4.486-10,10-10h352c5.514,0,10,4.486,10,10V364.189z"/><path d="M232,197.232H120c-5.522,0-10,4.477-10,10s4.478,10,10,10h112c5.522,0,10-4.477,10-10S237.522,197.232,232,197.232z"/><path d="M232,244.768H120c-5.522,0-10,4.477-10,10s4.478,10,10,10h112c5.522,0,10-4.477,10-10S237.522,244.768,232,244.768z"/><path d="M232,294.768H120c-5.522,0-10,4.477-10,10s4.478,10,10,10h112c5.522,0,10-4.477,10-10S237.522,294.768,232,294.768z"/></g></svg>';
				html+='<svg class="icon startButton" version="1.1" id="startButton"  xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve"><g>	<g><path d="M496,112h-48c-4.253-0.025-8.34,1.645-11.36,4.64L409.44,144H384v-16c0-26.51-21.49-48-48-48H48c-26.51,0-48,21.49-48,48v256c0,26.51,21.49,48,48,48h288c26.51,0,48-21.49,48-48v-16h25.44l27.36,27.36c2.98,2.956,7.003,4.622,11.2,4.64h48c8.837,0,16-7.163,16-16V128C512,119.163,504.837,112,496,112z M480,368h-25.44l-27.36-27.36c-2.98-2.956-7.003-4.622-11.2-4.64h-48c-8.837,0-16,7.163-16,16v32c0,8.837-7.163,16-16,16H48c-8.837,0-16-7.163-16-16V128c0-8.837,7.163-16,16-16h288c8.837,0,16,7.163,16,16v160h32V176h32c4.253,0.025,8.341-1.645,11.36-4.64l27.2-27.36H480V368z"/></g></g></svg>';
				if (usertype=='tokenuser' && tokenAgent == name){
					html+='</div><div class="body"><input type="text" class="input" id="replyfield"/><input type="file" class="input" id="uploadfile" style="visibility:hidden;"/></div>';
					html+='<div class="foot"><a class="send" href="javascript:void(0)" id="replybtn">Send(Enter)</a></div></div></div></div></div>';
				}else{
					html+='</div><div class="body"><input type="text" class="input" id="replyfield" disabled="disabled"/><input type="file" class="input" id="uploadfile" style="visibility:hidden;"/></div>';
					html+='<div class="foot"><a class="send isDisabled" href="javascript:void(0)" id="replybtn">Send(Enter)</a></div></div></div></div></div>';
				}
				$('#chat').append(html);
			}
		}

		function sendMessage(head, val,name){
			var ts = new Date;
			$('.chat-active .chat-msg').append('<li><div class="icon"><img class="mehead" src="'+head+'"><span class="help-block">'+name+'</span></div><span class="mespeak">'+val+'</span><p class="help-block" style="float:right;">'+ts.toLocaleTimeString()+'</p></li>');
		}

		function loadMessage(head, val){
				//console.log("to client shown");
			var ts = val.date || new Date;
			console.log(hex_md5(val.recipient));
			$('#'+hex_md5(val.recipient)+' .chat-msg').append('<li><div class="icon"><img class="mehead" src="'+head+'"><span class="help-block">'+(val.addresser ? val.addresser : 'chatbot')+'</span></div><span class="mespeak">'+val.body+'</span><p class="help-block" style="float:right;">'+ (val.date ? ts : ts.toLocaleTimeString())+'</p></li>');
		}
		function loadMessagefromClient(head, val){
		//	console.log("from client shown");
			$('#'+hex_md5(val.addresser)+' .chat-msg').append('<li><img src="'+head+'"><span class="speak">'+val.body+'</span><p class="help-block">'+val.date+'</p></li>');
		}

		// function receiveMessage(head,val){
		// 	$('.chat-active .chat-msg').append('<li><img src="'+head+'"><span class="speak">'+val+'</span></li>');
		// }

		function join_chat_channel(channel, userdata) {
      socket.emit('join', {"channel": channel, "userdata": userdata});
    }
    function part_chat_channel(channel) {
      socket.emit('part', channel);
    }

		function fileTooBig(data){
			var fileSize = data.size/1024/1024; //MB
			var File_Size_Limit = 15;
			if (fileSize > File_Size_Limit){
					alert("Don't upload file larger than "+File_Size_Limit+" MB!");
					return true;
			}
			return false;
		}

		function updateAgentList(agents){
			var box = $('#transferAgent .radio-row');
			var html='';
			agents.forEach(function(agent, index, arr){
				if (agent !== name) {
					html += '<label class="radio-container m-r-45">True<input type="radio" value="true" name="hasfile"><span class="radio-checkmark"></span></label>';
				}
				if (index == arr.lenght-1){
					$('#transferAgent .radio-row').append(html);
				}
			})
		}

		function scrollToBottom(root){
			$('#'+root+' .body').scrollTop($('#'+root+' .chat-msg').height());
		}

		$('#emoji span').click(function(){
			var val = $('.chat-active input[type=text]').val();
			$('.chat-active input[type=text]').val(val+$(this).text());
			$('#emoji').css('display','none');
		});

		$('#demands span').click(function(){
			var recipient = $('.chat-active').attr('data-n');
			if($(this).text() == '身份證') {
				var val='<input type="file" class="fileinput" id="idcard" name="idcard" accept="image/png, image/jpeg" data-multiple-caption="{count} files selected" onchange="handleFile(event,this.files)" multiple><label for="idcard"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 4px;"></i><strong>Choose your ID card photo</strong></label>';
			} else {
					var val='<input type="file" class="fileinput" id="addressproof" name="addressproof" accept="image/png, image/jpeg" onchange="handleFile(event,this.files)" data-multiple-caption="{count} files selected" multiple><label for="addressproof"><i class="fa fa-upload" aria-hidden="true" style="margin-right: 4px;"></i><strong>Choose your address proof</strong></label>';
				}
			sendMessage('src/img/head.jpg',val,name);
					//call
			// var val='<label for="idcard">Choose your ID card photo:</label><input type="file" id="idcard name="idcard" accept="image/png, image/jpeg">'
			var req = {
				'addresser':name,
				'recipient':recipient,
				'type':'plain',
				'body':val
			}
			socket.emit('send private message', req);
			$('#demands').fadeOut();
		})

		function notifications(amount){
			console.log("amount::: ", amount);
			if ( $('#requestnotic .counter').length ){
				console.log("counter element exist", $('.counter').length);
				if (amount == 0){
					$('#requestnotic .counter').remove();
				}else{
					$('#requestnotic .counter').text(amount);
				}
			}else{
				console.log("counter element not exist");
				if (amount !== 0){
					console.log("adding counter element");
					$('#requestnotic').prepend('<span class="counter bgc-red">'+amount+'</span>');
				}
			}

		}

		function showNotice(head,title,msg){
			var Notification = window.Notification || window.mozNotification || window.webkitNotification;
			if(Notification){
				Notification.requestPermission(function(status){
					//status=default=denied,'granted'=notification open
					if("granted" != status){
						return;
					}else{
						var tag = "sds"+Math.random();
						var notify = new Notification(
							title,
							{
								dir:'auto',
								lang:'zh-CN',
								tag:tag,//notification id
								icon:'/'+head,
								body:msg
							}
						);
						notify.onclick=function(){
							//open window
							window.focus();
						},
						notify.onerror = function () {
							console.log("HTML5 error！！！");
						};
						notify.onshow = function () {
							setTimeout(function(){
								notify.close();
							},2000)
						};
						notify.onclose = function () {
							console.log("HTML5 window close！！！");
						};
					}
				});
			}else{
				console.log("Browser Not Supported");
			}
		};
})
