const
	fs   = require('fs'),
	url  = require('url'),
	path = require('path'),
	http = require('http'),
	{ parse } = require('querystring'),
	MongoClient = require('mongodb').MongoClient,
	hostname = '127.0.0.1',
	port = 3001;
	var Binary = require('mongodb').Binary;
	var formidable = require('formidable');
	//Set Cookies
	var Cookies = {};

//mysql confifuration and connection
// var connection = mysql.createConnection({
//   host     : 'localhost',
//   user     : 'root',
//   password : '',
// 	database : 'cti_project',
// 	port: '3306',
// });
// connection.connect();

var options = {
    server: {
        auto_reconnect: true,
        poolSize: 10,
				// retry to connect for 60 times
        reconnectTries: 60,
        // wait 1 second before retrying
        reconnectInterval: 1000
    }
};

var defaultPage = ['index.html','default.html','index.htm','default.htm'];

var fsroot = path.join(path.resolve(process.argv[2] || '.'),'/www');

const server = http.createServer((request, response) => {



    var pathname = url.parse(request.url).pathname;
		// console.log("pathname:: ", pathname);
    var filepath = path.join(fsroot, pathname);
		if(pathname == '/cookies'){
					response.end(JSON.stringify(Cookies))
				}
    if(pathname == '/') {
    	for(var i=0;i<defaultPage.length;i++){
			tempfilepath = path.join(filepath, '/'+defaultPage[i]);
			var stats = fs.statSync(tempfilepath);
			if(stats.isFile()){
				filepath = tempfilepath;
				console.log('default page: '+filepath);
				break;
			}
    	}
		}
		if(pathname == '/permission'){
			console.log(Cookies)
			let data = JSON.stringify(Cookies)
			console.log("data: " + data)
			response.end(JSON.stringify(Cookies))
		}
		if(pathname == '/logout'){
			Cookies = {}
			response.end("logout")
		}
		if(pathname == '/login'){
			request.on("data", (data)=>{
				data = data.toString();
				console.log(data)
				MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
					if (err) console.log(err);
					client.db('CES').collection('user').find({'password': data}).toArray(function(err, results){
						if (err) console.log(err);
						console.log(results);
						if(results.length <= 0){
							console.log("result with error::: ", err)
							response.end('error')
						}else{
							Cookies['username'] = results[0].email
							Cookies['permission'] = results[0].permission
							console.log("Cookies:: ", Cookies)
							response.end(Cookies.permission)
						}
					});
				});
			})
		}
		//
		// if(pathname == '/filtering-search'){
		// 	request.on('data', function(data){
		// 		var query = data.toString();
		// 		console.log(parse(query));
		// 		response.end(parse(query));
		// 	})
		// }

		if (pathname == '/v1/uploadDoc'){
			console.log('uploading');
			request.on('data', function(data){
				console.log("data in server:: ", data);
				var imgdatetimenow = Date.now();
				var form = new formidable.IncomingForm({
			      	uploadDir: __dirname + '/public/upload/doc',
			      	keepExtensions: true
			      });

				form.on('end', function() {
			      res.end();
			    });
			    form.parse(data,function(err,fields,files){
					var data = {
							repeatMsg : true,
							isPDFFile : fields.isPDFFile,
							istype : fields.istype,
							dwimgsrc : fields.dwimgsrc,
							serverfilename : baseName(files.file.path),
							msgTime : fields.msgTime,
							filename : files.file.name,
							size : bytesToSize(files.file.size)
					};
				    var pdf_file = {
					        filename : files.file.name,
					        filetype : fields.istype,
					        serverfilename : baseName(files.file.path),
					        serverfilepath : files.file.path,
					        expirytime : imgdatetimenow + (3600000 * expiryTime)
				    };
				    files_array.push(pdf_file);
				//	ios.sockets.emit('new message PDF', data);
			    });
			})
		}

		if (pathname == '/gethistorytable'){
			request.on('data', function(cond){
				var d = cond.toString();
				var query={};
				var platform;
				var date='';
				var conds;
				if (d == 'initial') {
					query = {};
				}else{
					d = parse(d);
					// query = parse(d);
					console.log('d:: ',d);
					var start_day = new Date(d.datefrom);
					var end_day = new Date(d.dateto);
					console.log("start: ", d.datefrom, ' ', start_day);
					console.log("end: ", d.dateto, ' ', end_day);
					if (d.platform == 'default'){
						platform = '';
					}else{
						query.service = d.platform;
						platform = '"service":"'+d.platform+'"';
					}
					if (d.livechat == 'true'){
						if (d.hasfile == 'default' && d.hascall == 'default'){
								conds = '"livechat":"true"';
								query.livechat = 'true';
						}else if(d.hasfile == 'default' && d.hascall !== 'default'){
							conds =  '"livechat":"true","isVideo":"'+d.hascall+'"';
							query.livechat = 'true';
							query.isVideo = d.hascall;
						}else if(d.hasfile !== 'default' && d.hascall == 'default'){
							conds =  '"livechat":"true","isFile":"'+d.hasfile+'"';
							query.livechat = 'true';
							query.isFile = d.hasfile;
						}else{
							conds =  '"livechat":"true","isFile":"'+d.hasfile+'","isVideo":"'+d.hascall+'"';
						}
					}else if(d.livechat == 'false'){
						conds = '"livechat":"false"';
						query.livechat = 'false';
					}else{
						conds = '';
					}
					if (d.datefrom !== d.dateto){
						query.date = {};
						query.date.$gte = new Date(d.datefrom);
						query.date.$lt = new Date(d.dateto);
					}
					// if (platform == '' && conds == ''){
					// 	// if ( date.length > 1) date = date.substring(1);
					// 	// query = '{'+date+'}';
					// }else if (platform == ''){
					// 	query = '{'+conds;
					// }else if (conds == ''){
					// 	query =  '{'+platform;
					// }else{
					// 	query = '{'+platform+','+conds;
					// }
					// // query += '{'+platform+conds+'}';
					// query = query+',"date": {"$gte":'+start_day+',"$lt":'+end_day+'}}'
					// query = JSON.parse(query);
				}

				MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
					if (err) console.log(err);

					const db = client.db('DataRecord');
					console.log(query);
					db.collection('requests').find(query).toArray(function(err, data){
						//console.log(data);
						data = JSON.stringify(data);
						response.writeHead(200, {"Content-Type":"application/json","Content-Length":data.length});
						response.end(data);
						client.close();
					})
				})
			})


		}
		if (pathname == '/gethistorychat'){
			//var chathistory ='';
			request.on('data',function(data){
			//	console.log(data.toString());
				MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err,client){
					if (err) console.log(err);
					const db = client.db("ChatRecord");
					db.collection(data.toString()).find().toArray(function(err, doc){
						if (err) console.log(err);
					//	chathistory = doc;
						response.end(JSON.stringify(doc));
						console.log(doc.length);
					//	console.log(JSON.stringify(doc).length);
						client.close();
					})
				});
				// request.on('end', function(){
				// 		console.log(chathistory);
				// 		response.writeHead(200, {"Content-Type":"application/json","Content-Length":chathistory.length});
				// 		response.end(JSON.stringify(chathistory));
				// });
			});
		}
		if (pathname == '/getavgResTime'){
			MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
				if (err) console.log(err);
				var diffs = [], total=0;
				// client.db('DataRecord').collection('requests').aggregate( [{$project: { duration: {$divide: [{$subtract: ["$tokentime", "$requesttime"]}, 3600000]} }}] ).toArray(function(err, results){;
				//client.db('DataRecord').collection('requests').aggregate([ { $project: { starttime: { $dateFromString: { dateString: '$requesttime'}}, endtime: { $dateFromString: { dateString: '$tokentime'}}, duration: { $divide: [{$subtract: ['$endtime', '$starttime']},3600000]}}}]).toArray(function(err, results){
				client.db('DataRecord').collection('requests').find({'livechat':'true','service':'Website','tokentime':{$ne : null}}).toArray(function(err, results){
					results.forEach(function(result, ridx, array){
						// console.log(result);
						if (result.tokentime == '' || result.tokentime == null){

						}else{
							var date1 = new Date(String(result.date).substring(0,10)+' '+result.requesttime);
							var date2 = new Date(String(result.date).substring(0,10)+' '+result.tokentime);
							// console.log(result.tokentime);
							diffs.push((date2.getTime() - date1.getTime())/60000);
							// console.log(date2.getTime() - date1.getTime());
							total += (date2.getTime() - date1.getTime())/60000;
							// console.log(total);
						}
						if (ridx === array.length - 1){
							var avg = total / diffs.length;
							var string = "m";
							if ( avg < 1) {
								avg = (avg * 60).toFixed(2);
								string = "s";
							}else{
								avg = avg.toFixed(2);
							}
							// console.log("average time:: ", avg, "total:: ", total);
							response.end(avg.toString()+string);
							client.close();
						}
					});
				})
				//
				// var total = 0;
				// for(var i = 0; i < diffs.length; i++){
				// 	total +=diffs[i];
				// }
				// var avg = total / diffs.length;
				// console.log("average time:: ", avg);
				// response.end(avg);
				// client.close();
			})
		}
		if (pathname == '/getdashboard'){
			MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
				if (err) console.log(err);
				client.db('DataRecord').collection('requests').find({}).count().then( res =>{
					var data = {
						'serving' :  user.length,
						'liveServing' : token.length,
						'served' : res
					}
					console.log(data);
					data = JSON.stringify(data);
					response.writeHead(200, {"Content-Type":"application/json","Content-Length":data.length});
					response.end(data);
					client.close();
				});
			})
		}

	fs.stat(filepath, (err,stats) => {
		if(!err && stats.isFile()){
			//file
			console.log(request.method +' 200 ' + request.url);
    		response.statusCode = 200;
			fs.createReadStream(filepath).pipe(response);
		}else{
			if (pathname == '/'){
			// if (pathname != '/gethistorytable' || pathname != '/gethistorychat'){
				console.log(request.method +' 404 ' + request.url);
				response.statusCode = 404;
				response.setHeader('Content-Type', 'text/plain');
				response.end('404 Not Found');
			}

		}
	});
});

server.listen(port, () => {
  console.log(`Server running at http://localhost:${port}/`);
});

const io = require('socket.io')(server,{ pingInterval: 10000, pingTimeOut:5000});

//JSON.stringify();
var usocket = {}, user = [], asocket={}, agent = [], queuing=[], token=[], tokenAgent={}, wechat={};
var channels = {};
var sockets = {};
var files = {},
	struct = {
			name: null,
			type: null,
			size: 0,
			data: [],
			slice: 0,
	};

io.on('connection', (socket) => {

	var historyid=[], history = {};
	socket.channels = {};
	sockets[socket.id] = socket;

	socket.on('new agent', (agentname) => {
		console.log("agent name:: ", agentname);
		if(!(agentname in asocket)) {
			agent.push(agentname);
			socket.agentname = agentname;
			asocket[agentname] = socket;
			socket.emit('login',agentname, user, queuing, token, tokenAgent,agent);
			console.log(agent);
			agent.forEach((a) =>{
				if (a !== agentname)
					asocket[a].emit('update agent list', agent);
			})
		}
	})

	socket.on('new client', (username, via, userid, usericon) => {
		if(!(username in usocket)) {
			var ts = new Date;
			var service = (via == null) ? "Website" : via;
			var platformid = (via == null) ? '' : userid;
			var clienticon = (via == null) ? '' : usericon;
			if (via !== null){
			    wechat[username] = usericon;
			}
			//console.log("client from ", service);
			MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
				if (err) console.log(err);
				const db = client.db('ChatRecord');
				db.createCollection(username, function(err, result){
					if (err) console.log(err);
				});
				client.db('DataRecord').collection('requests').insert(
					{
  						clientid: username,
  						date: new Date(),
  						starttime: ts.toLocaleTimeString(),
  						service: service,
  						platformid: platformid,
  						livechat: 'false',
  						requesttime: '',
  						agent: '',
  						tokentime: '',
  						hanguptime:'',
  						endtime: '',
  						isFile: 'false',
  						isVideo: 'false',
                        dialogue: 0,
                        misunderstanding :0,
                        understanding:0
  					}, function(err, result){
						if (err) console.log(err);
						client.close();
					}
				)

			});
			socket.username = username;
			usocket[username] = socket;
			user.push(username);
			historyid.push(username);
			socket.emit('login',user);
			socket.broadcast.emit('client joined',username,(user.length-1),user,queuing.length+token.length+user.length, clienticon);
			console.log(user);
		}
	})
	
	socket.on('misunderstanding', function(clientid){
      MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
        if (err) console.log(err);
        const db = client.db('DataRecord');
        db.collection('requests').update({'clientid': clientid}, {$inc:{misunderstanding:1}}, function(err, result){
          if (err) console.log(err);
          client.close();
        })
      })
    })

    socket.on('understanding', function(clientid){
      // console.log(clientid);
      MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
        if (err) console.log(err);
        const db = client.db('DataRecord');
        db.collection('requests').update({'clientid': clientid}, {$inc:{understanding:1}}, function(err, result){
          if (err) console.log(err);
          client.close();
        })
      })
    })

	socket.on('new request', (username) => {
		console.log("receive request from ", username);
		queuing.push(username);
		var ts = new Date;
		for (var i=0; i<user.length; i++){
			if (user[i]==username){
				user.splice(i,1);
			}
		}
		MongoClient.connect("mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/", function(err, client){
			if (err) console.log(err);
			const db = client.db("DataRecord");
			db.collection("requests").update({'clientid':username},{$set:{'livechat':'true', 'requesttime':ts.toLocaleTimeString()}}, function(err, result){
				if (err) console.log(err);
			//	console.log(result);
				client.close();
			})
		});
		agent.forEach((agent) => {
			asocket[agent].emit('load request', username, user, queuing);
		})
	});

	socket.on('takeover', function(res){
		console.log(res);
	//	console.log("before:: ", agent[agent.indexOf(res.addresser)]);
	//	agent[agent.indexOf(res.addresser)].client=[];
	//	agent[agent.indexOf(res.addresser)].client.push(";;;");
	//	console.log("after:: ", agent[agent.indexOf(res.addresser)].client);
		// if(!agent[agent.indexOf(res.addresser)].client || agent[agent.indexOf(res.addresser)].client == undefined) agent[agent.indexOf(res.addresser)].client=[];
		if(res.recipient in usocket) {
			var ts = new Date;
			if(res.status=='takeover'){
				token.push(res.recipient);
				tokenAgent[res.recipient] = res.addresser;
				// agent[agent.indexOf(res.addresser)].client.push(res.recipient);
				if (user.includes(res.recipient)) user.splice(user.indexOf(res.recipient), 1);
				if (queuing.includes(res.recipient)) queuing.splice(queuing.indexOf(res.recipient), 1);
				MongoClient.connect("mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/", function(err, client){
					if (err) console.log(err);
					const db = client.db("DataRecord");
					db.collection("requests").update({'clientid':res.recipient},{$set:{'agent':res.addresser,'tokentime':ts.toLocaleTimeString()}}, function(err, result){
						if (err) console.log(err);
				//		console.log(result);
						client.close();
					})
				});
				// for (var i=0; i<queuing.length; i++){
				// 	if (queuing[i]==res.recipient){
				// 		queuing.splice(i,1);
				// 	}
				// }
			}else if (res.status == 'hangup') {
				user.push(res.recipient);
				delete tokenAgent[res.recipient];
				for (var i=0; i<token.length; i++){
					if (token[i]==res.recipient){
						token.splice(i,1);
					}
				}
				MongoClient.connect("mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/", function(err, client){
					if (err) console.log(err);
					const db = client.db("DataRecord");
					db.collection("requests").update({'clientid':res.recipient},{$set:{'hanguptime':ts.toLocaleTimeString()}}, function(err, result){
						if (err) console.log(err);
				//		console.log(result);
						client.close();
					})
				});
			}

			usocket[res.recipient].emit('agent received', res);
			agent.forEach((agent) => {
				if (res.addresser != agent) {
					asocket[agent].emit('prevent duplicate take', res, user, queuing, token, tokenAgent);
				}else{
					asocket[agent].emit('refresh amount', user, queuing, token);
				}
			})
		}
	})

	socket.on('send chatbot message', function (data){
		//	console.log("access in server.js in server");
		socket.broadcast.emit('load chatbot message', data);
		var ts = new Date;
		var collectionname = data.recipient;
		var doc={
			addresser: 'chatbot',
			recipient: data.recipient,
			body: data.body,
			type: 'text',
			date: ts.toLocaleString()
		};
		MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
			if (err) console.log(err);
			const db = client.db('ChatRecord');
			db.collection(collectionname).insert(doc, function(err, result){
				if (err) console.log(err);
			//	console.log(result);
				client.close();
			});
		});
	});

	socket.on('send public message', function (data){
		var ts = new Date;
		var collectionname = data.addresser;
		var doc={
			addresser: data.addresser,
			recipient: 'chatbot',
			body: data.body,
			type: 'text',
			date: ts.toLocaleString()
		};
		var icon = (data.addresser in wechat) ? wechat[data.addresser] : null;
		console.log(icon);
		agent.forEach((agent) => {
			asocket[agent].emit('receive private message', data, icon);
		});
		MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
			if (err) console.log(err);
			const db = client.db('ChatRecord');
			db.collection(collectionname).insert(doc, function(err, result){
				if (err) console.log(err);
			//	console.log(result);
				client.close();
			});
		});
	})

	socket.on('send private message', function(res){
		console.log(res);
		var ts = new Date;
		var collectionname;
		var doc={
			addresser: res.addresser,
			recipient: res.recipient,
			type: 'text',
			body: res.body,
			date: ts.toLocaleString()
		}
		if(res.recipient in usocket) {
			collectionname = res.recipient;
			usocket[res.recipient].emit('receive private message', res);
		}else if(res.recipient in asocket) {
			collectionname = res.addresser;
			console.log("send to agent");
			if (res.addresser in wechat){
			    console.log("have icon:: ", wechat[res.addresser]);
		    	asocket[res.recipient].emit('receive private message', res, wechat[res.addresser]);
			}else{
		    	asocket[res.recipient].emit('receive private message', res, null);
			}
		}
		MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
			if (err) console.log(err);
			const db = client.db('ChatRecord');
			db.collection(collectionname).insert(doc, function(err, result){
				if (err) console.log(err);
			//	console.log(result);
				client.close();
			});
		});
	});

	socket.on('slice upload', (data) => {
	    if (!files[data.name]) {
	        files[data.name] = Object.assign({}, struct, data);
	        files[data.name].data = [];
	    }

	    //convert the ArrayBuffer to Buffer
	    data.data = new Buffer(new Uint8Array(data.data));
	    //save the data
	    files[data.name].data.push(data.data);
	    files[data.name].slice++;
			// asocket[data.recipient].emit('wait upload finish', socket.username);
			console.log(data);
	    if (files[data.name].slice * 100000 >= files[data.name].size) {
				var fileBuffer = Buffer.concat(files[data.name].data);
				console.log(socket.username);
				data.addresser = socket.username;
				fs.writeFile('www/public/upload/'+data.name, fileBuffer, (err) => {
						delete files[data.name];
						if (err) return socket.emit('upload error');
						asocket[data.recipient].emit('end upload', data);
						var ts = new Date;
						var doc={
							addresser: socket.username,
							recipient: data.recipient,
							type: 'image',
							body: data.name,
							date: ts.toLocaleString()
						};
						MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
							if (err) console.log(err);
							const db = client.db('ChatRecord');
							db.collection(socket.username).insert(doc, function(err, result){
								if (err) console.log(err);
								client.db("DataRecord").collection("requests").update({'clientid':socket.username},{$set:{'isFile':'true'}}, function(err, result){
									if (err) console.log(err);
									client.close();
								})
							});
						});
				});
	    } else {
					console.log(data.name);
	        usocket[socket.username].emit('request slice upload', {
							filename: data.name,
	            currentSlice: files[data.name].slice
	        });
	    }
	});


	socket.on('base64 file', function (msg) {
    console.log('sent base64 file to '+msg.recipient+' by ' + msg.addresser);
		if (msg.recipient in usocket){
			console.log("file to client");
			usocket[msg.recipient].emit('base64 file', msg);
			asocket[msg.addresser].emit('file sent');
			var collectionname = msg.recipient;
		}else if (msg.recipient in asocket){
			console.log("file to agent");
			asocket[msg.recipient].emit('base64 file', msg);
			var collectionname = msg.addresser;
		}
		var ts = new Date;
		var doc={
			addresser: msg.addresser,
			recipient: msg.recipient,
			type: 'file',
			body: msg.fileName,
			filecontent: msg.file,
			date: ts.toLocaleString()
		};
		MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
			if (err) console.log(err);
			const db = client.db('ChatRecord');
			db.collection(collectionname).insert(doc, function(err, result){
				if (err) console.log(err);
				client.db("DataRecord").collection("requests").update({'clientid':collectionname},{$set:{'isFile':'true'}}, function(err, result){
					if (err) console.log(err);
					client.close();
				})
			});
		});
  //  socket.username = msg.username;
    // socket.broadcast.emit('base64 image', //exclude sender
    // io.sockets.emit('base64 file',  //include sender
		//
    //     {
    //       username: socket.username,
    //       file: msg.file,
    //       fileName: msg.fileName
    //     }
		//
    // );
	});

	socket.on('join', function (config) {
      console.log("["+ socket.id + "] join ", config);
      var channel = config.channel;
      var userdata = config.userdata;

      if (channel in socket.channels) {
          console.log("["+ socket.id + "] ERROR: already joined ", channel);
          return;
      }

      if (!(channel in channels)) {
          channels[channel] = {};
      }

      for (id in channels[channel]) {
          channels[channel][id].emit('addPeer', {'peer_id': socket.id, 'channel': channel, 'should_create_offer': false});
          socket.emit('addPeer', {'peer_id': id,  'channel': channel, 'should_create_offer': true});
      }

      channels[channel][socket.id] = socket;
      socket.channels[channel] = channel;
  });

	socket.on('relayICECandidate', function(config) {
        var peer_id = config.peer_id;
        var ice_candidate = config.ice_candidate;
        console.log("["+ socket.id + "] relaying ICE candidate to [" + peer_id + "] ", ice_candidate);

        if (peer_id in sockets) {
            sockets[peer_id].emit('iceCandidate', {'peer_id': socket.id, 'ice_candidate': ice_candidate});
        }
    });

	socket.on('relaySessionDescription', function(config) {
      var peer_id = config.peer_id;
      var session_description = config.session_description;
      console.log("["+ socket.id + "] relaying session description to [" + peer_id + "] ", session_description);

      if (peer_id in sockets) {
          sockets[peer_id].emit('sessionDescription', {'peer_id': socket.id, 'session_description': session_description});
      }
  });

	function part(data) {
        console.log("["+ socket.id + "] part ");

        if (!(data.channel in socket.channels)) {
            console.log("["+ socket.id + "] ERROR: not in ", data.channel);
            return;
        }

				var peerSocket = data.recipient;
				if ( peerSocket in usocket) {
					console.log("agent hangup");
					usocket[peerSocket].emit('vcallhangup',{'channel': data.channel});
					// delete usocket[peerSocket].channels[data.channel];
					// delete channels[data.channel][usocket[peerSocket].id];
				}
				if ( peerSocket in asocket){
					console.log("client hangup");
					asocket[peerSocket].emit('vcallhangup',{'channel': data.channel});
					// delete channels[data.channel][usocket[peerSocket].id];
					// delete asocket[peerSocket].channels[data.channel];
				}
        delete socket.channels[data.channel];
        delete channels[data.channel][socket.id];

        for (id in channels[data.channel]) {
					// delete channels[data.channel][id];
            channels[data.channel][id].emit('removePeer', {'peer_id': socket.id});
            socket.emit('removePeer', {'peer_id': id});
        }
    }
  socket.on('part', part);

	socket.on('video calling', function(data){
		if (data.recipient in usocket){
			usocket[data.recipient].emit('video call request', data);
			var collectionname = data.recipient;
		}else{
			asocket[data.recipient].emit('video call request', data);
			var collectionname = data.addresser;
		}
		var ts = new Date();
		var doc={
			addresser: data.addresser,
			recipient: data.recipient,
			type: 'call',
			body: data.addresser+' start a video call',
			date: ts.toLocaleString()
		};
		MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', options, function(err, client){
			if (err) console.log(err);
			const db = client.db('ChatRecord');
			db.collection(collectionname).insert(doc, function(err, result){
				if (err) console.log(err);
				client.db("DataRecord").collection("requests").update({'clientid':collectionname},{$set:{'isVideo':'true'}}, function(err, result){
					if (err) console.log(err);
					client.close();
				})
			});
		});

	});

	socket.on('disconnect', function(reason){
		console.log("Socket Disconnect:: ", reason);
		for (var channel in socket.channels) {
        part(channel);
    }
    delete sockets[socket.id];
		if(socket.username in usocket){
			var ts = new Date;
			delete(usocket[socket.username]);
			delete(tokenAgent[socket.username]);
			if (user.includes(socket.username)) user.splice(user.indexOf(socket.username), 1);
			if (queuing.includes(socket.username)) queuing.splice(queuing.indexOf(socket.username), 1);
			if (token.includes(socket.username)) token.splice(token.indexOf(socket.username), 1);
			console.log("Online user:: ", user);
			MongoClient.connect('mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/', function(err, client){
				if (err) console.log(err);
				const db = client.db('ChatRecord');
				db.collection(socket.username).find().toArray(function(err, doc){
					if (err) console.log(err);
					var docs = {};
					docs[socket.username] = doc;
				//	console.log(docs);
					socket.broadcast.emit('client left',socket.username,user, queuing, token, socket.username, docs);
				})

				client.db("DataRecord").collection("requests").update({'clientid':socket.username},{$set:{'endtime':ts.toLocaleTimeString()}}, function(err, result){
					if (err) console.log(err);
					// console.log(result);
					client.close();
				})
			})

		}else if (socket.agentname in asocket){
			if (agent[agent.indexOf(socket.agentname)].client && agent[agent.indexOf(socket.agentname)].client.length){
				for (var tclient in agent[agent.indexOf(socket.agentname)].client){
					user.push(tclient);
					token.splice(token.indexOf(tclient),1);
					MongoClient.connect("mongodb://katiao:katiao0820@mongodb-1872-0.cloudclusters.net:10007/", function(err, client){
						if (err) console.log(err);
						const db = client.db("DataRecord");
						db.collection("requests").update({'clientid':tclient},{$set:{'hanguptime':ts.toLocaleTimeString()}}, function(err, result){
							if (err) console.log(err);
					//		console.log(result);
							client.close();
						})
					});
				}
			}
			delete(asocket[socket.agentname]);
			agent.splice(agent.indexOf(socket.agentname),1);
		}

	})

});
